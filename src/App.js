import React, {useEffect, useState} from 'react';
import {View, ScrollView} from 'react-native';
// import CallApiAxios from './pages/CallApiAxios';
// import Sample from './pages/Sample';
// import StylingReactNativeComponent from './pages/StylingReactNativeComponent';
// import Flexbox from './pages/FlexBox';
// import PositionReactNative from './pages/PositionReactNative';
// import PropsDinamis from './pages/PropsDinamis';
// import StateDinamis from './pages/StateDinamis';
// import Communication from './pages/Communication';
// import ReactNativeSVG from './pages/ReactNativeSVG';
// import BasicJavascript from './pages/BasisJavascript';
// import CallApiVanilla from './pages/CallApiVanilla';
import LocalAPI from './pages/LocalAPI';
import ToDoList from './pages/ToDoList';

const App = () => {
  // const [isShow, SetIsShow] = useState(true);
  // useEffect(() => {
  //   setTimeout(() => {
  //     SetIsShow(false);
  //   }, 6000);
  // });
  return (
    <View>
      <ScrollView>
        {/* <Sample />
        <StylingReactNativeComponent /> */}
        {/* {isShow && <Flexbox />} */}
        {/* <Flexbox /> */}
        {/* <PositionReactNative /> */}
        {/* <PropsDinamis />
        <StateDinamis /> */}
        {/* <Communication /> */}
        {/* <ReactNativeSVG /> */}
        {/* <BasicJavascript /> */}
        {/* <CallApiVanilla /> */}
        {/* <CallApiAxios /> */}
        {/* <LocalAPI /> */}
        <ToDoList />
      </ScrollView>
    </View>
  );
};

export default App;
