import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const CustomButton = ({onPress, color, text}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={styles.buttonContainer(color)}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  buttonContainer: color => ({
    flex: 1,
    padding: 20,
    backgroundColor: color,
    borderRadius: 50,
    width: 70,
    height: 70,
  }),
  text: {textAlign: 'center', fontSize: 25},
});
