import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CustomButton from '../../components/CustomButton';

const ActivityList = ({
  color = 'green',
  text = '+',
  onPress,
  activity = 'Makan',
}) => {
  return (
    <View style={styles.listContainer}>
      <Text>{activity}</Text>
      <CustomButton color={color} text={text} onPress={onPress} />
    </View>
  );
};

export default ActivityList;

const styles = StyleSheet.create({
  listContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'yellow',
    marginVertical: 15,
  },
});
