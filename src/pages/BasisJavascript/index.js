import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const BasicJavascript = () => {
  // variable declaration
  // tipe data primitive
  const nama = 'Muhtarom Zain'; //string
  let usia = 24; //number
  const jenisKelamin = 'Laki laki'; //string
  const isMale = true; //boolean

  // complex
  const hewanPeliharaan = {
    nama: 'Cubu',
    jenis: 'Kucing',
    usia: 2,
    apakahHewanLokal: false,
    warna: 'Putih',
    orangTua: {
      betina: 'Belang',
    },
  }; // object

  const sapaOrang = (name, age) => {
    return console.log(`Hello ${name} usia anda ${age}`);
  }; // function

  const namaOrang = ['Muhtarom', 'Zain', 'Dita', 'Kintani']; // object - array (in typeof as object)

  const namaHewan = objectHewan => {
    let sapaan = '';
    if (hewanPeliharaan.nama === 'Cumbul') {
      sapaan = 'Hallo mbul';
    } else {
      sapaan = 'Siapa anda';
    }

    return sapaan;
  };

  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>Basic BasicJavascript</Text>
      <Text>{namaHewan(hewanPeliharaan)}</Text>
      {namaOrang.map(orang => (
        <Text>{orang}</Text>
      ))}
    </View>
  );
};

export default BasicJavascript;
const styles = StyleSheet.create({
  wrapper: {padding: 20},
  textTitle: {textAlign: 'center'},
});
