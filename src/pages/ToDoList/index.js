import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Button,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';

const Item = ({name, urgency, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity style={styles.desc} onPress={onPress}>
        <View style={styles.desc}>
          <Text style={styles.descNama}>{name}</Text>
          <Text style={styles.descUrgency}>Urgency Level: {urgency}</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>X</Text>
      </TouchableOpacity>
    </View>
  );
};

const ToDoList = () => {
  const [name, setName] = useState('');
  const [urgency, setUrgency] = useState('');
  const [button, setButton] = useState('Save');
  const [selectedUser, setSelectedUser] = useState({});

  useEffect(() => {
    getData();
  }, []);

  const [activities, setActivities] = useState([]);

  const submit = () => {
    const data = {
      name,
      urgency,
    };
    if (button === 'Save') {
      console.log('data before send: ', data);
      axios.post('http://10.0.2.2:3000/activities', data).then(res => {
        console.log('res: ', res);
        setName('');
        setUrgency('');
        getData();
      });
    } else if (button === 'Update') {
      axios
        .put(`http://10.0.2.2:3000/activities/${selectedUser.id}`, data)
        .then(res => {
          console.log('res update: ', res);
          setName('');
          setUrgency('');
          getData();
          setButton('Save');
        });
    }
  };

  const getData = () => {
    axios.get('http://10.0.2.2:3000/activities').then(res => {
      console.log('res: ', res);
      setActivities(res.data);
    });
  };

  const selectItem = item => {
    console.log('selected item: ', item);
    setSelectedUser(item);
    setName(item.name);
    setUrgency(item.urgency);
    setButton('Update');
  };

  const deleteItem = item => {
    console.log('deleted item: ', item);
    axios.delete(`http://10.0.2.2:3000/activities/${item.id}`).then(res => {
      console.log('res update: ', res);
      setName('');
      setUrgency('');
      setButton('Save');
      getData();
    });
  };

  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>To-Do-List (JSON Server)</Text>
      <TextInput
        placeholder="Activities; Coding, Swimming, Etc..."
        style={styles.input}
        value={name}
        onChangeText={value => setName(value)}
      />
      <TextInput
        placeholder="Urgency; Critical, Urgent, Etc..."
        style={styles.input}
        value={urgency}
        onChangeText={value => setUrgency(value)}
      />
      <Button title={button} onPress={submit} />
      {activities.length > 0 && (
        <View>
          <View style={styles.line} />
          <Text style={styles.textTitle}>List of Activities</Text>
        </View>
      )}
      {activities.map(activity => {
        return (
          <Item
            key={activity.id}
            name={activity.name}
            urgency={activity.urgency}
            onPress={() => selectItem(activity)}
            onDelete={() =>
              Alert.alert(
                'Caution',
                'Are you sure want to delete this activity?',
                [
                  {text: 'No', onPress: () => console.log('Button no')},
                  {text: 'Yes', onPress: () => deleteItem(activity)},
                ],
              )
            }
          />
        );
      })}
    </View>
  );
};

export default ToDoList;
const styles = StyleSheet.create({
  wrapper: {padding: 20},
  textTitle: {textAlign: 'center', marginBottom: 20, fontSize: 20},
  line: {backgroundColor: 'black', height: 2, marginVertical: 20},
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  avatar: {width: 80, height: 80, borderRadius: 80},
  itemContainer: {
    flexDirection: 'row',
    marginBottom: 30,
  },
  desc: {marginLeft: 18, flex: 1},
  descNama: {fontSize: 20, fontWeight: 'bold'},
  descUrgency: {fontSize: 16},
  descBidang: {fontSize: 12, marginTop: 8},
  delete: {fontSize: 25, fontWeight: 'bold', color: 'red'},
});
