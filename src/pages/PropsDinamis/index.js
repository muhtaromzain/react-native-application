import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';

const Story = props => {
  return (
    <View style={{alignItems: 'center', marginRight: 20}}>
      <Image
        source={{
          uri: props.image,
        }}
        style={{width: 50, height: 50, borderRadius: 50 / 2}}
      />
      <Text style={{maxWidth: 50, textAlign: 'center'}}>{props.title}</Text>
    </View>
  );
};

const PropsDinamis = () => {
  return (
    <View>
      <ScrollView horizontal>
        <View style={{flexDirection: 'row'}}>
          <Story
            title="Programming"
            image="https://yt3.ggpht.com/ytc/AAUvwnjrI1dxZo2MHAoJ18uw1Kyt3eTHLAeYrBz4WB7kNw=s88-c-k-c0x00ffffff-no-rj"
          />
          <Story
            title="Graphic Design"
            image="https://scontent-cgk1-1.cdninstagram.com/v/t51.2885-19/s320x320/106234838_4639247606101420_4052659588740354334_n.jpg?tp=1&amp;_nc_ht=scontent-cgk1-1.cdninstagram.com&amp;_nc_cat=104&amp;_nc_ohc=y13T6DeY-WUAX_YiM_d&amp;edm=ABfd0MgBAAAA&amp;ccb=7-4&amp;oh=7fedc203a8f0a78d6684d5ab0351be00&amp;oe=60C39821&amp;_nc_sid=7bff83"
          />
          <Story
            title="Programming"
            image="https://yt3.ggpht.com/ytc/AAUvwnjrI1dxZo2MHAoJ18uw1Kyt3eTHLAeYrBz4WB7kNw=s88-c-k-c0x00ffffff-no-rj"
          />
          <Story
            title="Design"
            image="https://scontent-cgk1-1.cdninstagram.com/v/t51.2885-19/s320x320/106234838_4639247606101420_4052659588740354334_n.jpg?tp=1&amp;_nc_ht=scontent-cgk1-1.cdninstagram.com&amp;_nc_cat=104&amp;_nc_ohc=y13T6DeY-WUAX_YiM_d&amp;edm=ABfd0MgBAAAA&amp;ccb=7-4&amp;oh=7fedc203a8f0a78d6684d5ab0351be00&amp;oe=60C39821&amp;_nc_sid=7bff83"
          />
          <Story
            title="Programming"
            image="https://yt3.ggpht.com/ytc/AAUvwnjrI1dxZo2MHAoJ18uw1Kyt3eTHLAeYrBz4WB7kNw=s88-c-k-c0x00ffffff-no-rj"
          />
          <Story
            title="Design"
            image="https://scontent-cgk1-1.cdninstagram.com/v/t51.2885-19/s320x320/106234838_4639247606101420_4052659588740354334_n.jpg?tp=1&amp;_nc_ht=scontent-cgk1-1.cdninstagram.com&amp;_nc_cat=104&amp;_nc_ohc=y13T6DeY-WUAX_YiM_d&amp;edm=ABfd0MgBAAAA&amp;ccb=7-4&amp;oh=7fedc203a8f0a78d6684d5ab0351be00&amp;oe=60C39821&amp;_nc_sid=7bff83"
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default PropsDinamis;
const styles = StyleSheet.create({});
