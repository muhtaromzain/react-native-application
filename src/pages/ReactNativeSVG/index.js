import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Logo from '../../assets/images/otw.svg';

const ReactNativeSVG = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>React Native SVG</Text>
      <Logo width={244} heigth={125} />
    </View>
  );
};

export default ReactNativeSVG;
const styles = StyleSheet.create({
  wrapper: {padding: 20},
  textTitle: {textAlign: 'center'},
});
