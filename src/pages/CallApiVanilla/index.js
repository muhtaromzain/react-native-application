import React, {useEffect, useState} from 'react';
import {Button, Image, StyleSheet, Text, View} from 'react-native';

const CallApiVanilla = () => {
  const [dataUser, setDataUser] = useState({
    avatar: '',
    email: '',
    first_name: '',
    last_name: '',
  });

  const [dataJob, setDataJob] = useState({
    name: '',
    job: '',
  });
  //   useEffect(() => {
  // Call API GET Method
  // fetch('https://reqres.in/api/users?page=2')
  //   .then(response => response.json())
  //   .then(json => console.log(json));
  // Call API POST Method
  // const data = {
  //   name: 'Muhtarom Zain',
  //   job: 'Software Engineer',
  // };
  // console.log(data);
  // console.log(JSON.stringify(data));
  // fetch('https://reqres.in/api/users', {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify(data),
  // })
  //   .then(response => response.json())
  //   .then(json => console.log(json));
  //   }, []);

  const getData = () => {
    fetch('https://reqres.in/api/users/2')
      .then(response => response.json())
      .then(json => {
        console.log(json);
        setDataUser(json.data);
      });
  };

  const postData = () => {
    const data = {
      name: 'Muhtarom Zain',
      job: 'Software Engineer',
    };

    fetch('https://reqres.in/api/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(json => {
        console.log(json);
        setDataJob(json);
      });
  };
  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>Call API</Text>
      <Button title="GET DATA" onPress={getData} />
      <Text>Response Get Data</Text>
      <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
      <Text>{`${dataUser.first_name} ${dataUser.last_name}`}</Text>
      <Text>{dataUser.email}</Text>
      <View style={styles.line} />
      <Button title="POST DATA" onPress={postData} />
      <Text>Response Post Data</Text>
      <Text>{dataJob.name}</Text>
      <Text>{dataJob.job}</Text>
    </View>
  );
};

export default CallApiVanilla;
const styles = StyleSheet.create({
  wrapper: {padding: 20},
  textTitle: {textAlign: 'center'},
  line: {backgroundColor: 'black', height: 2, marginVertical: 20},
  avatar: {width: 100, height: 100, borderRadius: 100},
});
