import React, {useEffect, useState} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';
import ActivityList from '../../components/ActivityList';
import CustomButton from '../../components/CustomButton';

const ProjectOne = () => {
  const [name, setName] = useState('');
  const [activity, setActivity] = useState([]);

  useEffect(() => {
    getData();
  });

  const getData = () => {
    axios.get('http://10.0.2.2:3000/activities').then(res => {
      console.log('res: ', res);
      setActivity(res.data);
    });
  };

  const submit = () => {
    const newData = {data};
    console.log(newData);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>To-Do Lists</Text>
      <View style={styles.formContainer}>
        <TextInput
          style={styles.form}
          placeholder="Activity, etc"
          value={data}
        />
        <Button title="+" onPress={submit} />
      </View>
      {activity.map(act => {
        return <ActivityList key={act} text="-" activity={act} />;
      })}
    </View>
  );
};

export default ProjectOne;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'red',
  },
  header: {fontSize: 42},
  formContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 40,
  },
  form: {
    width: '70%',
    borderWidth: 1,
    marginHorizontal: 20,
    borderRadius: 8,
  },
});
