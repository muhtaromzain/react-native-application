import axios from 'axios';
import React, {useState} from 'react';
import {Button, Image, StyleSheet, Text, View} from 'react-native';

const CallApiAxios = () => {
  const [dataUser, setDataUser] = useState({
    avatar: '',
    email: '',
    first_name: '',
    last_name: '',
  });

  const [dataJob, setDataJob] = useState({
    name: '',
    job: '',
  });

  const getData = () => {
    // fetch('https://reqres.in/api/users/2')
    //   .then(response => response.json())
    //   .then(json => {
    //     console.log(json);
    //     setDataUser(json.data);
    //   });
    axios
      .get('https://reqres.in/api/users/3')
      .then(result => {
        console.log('result: ', result);
        setDataUser(result.data.data);
      })
      .catch(err => console.log('err:', err));
  };

  const postData = () => {
    const data = {
      name: 'Muhtarom Zain',
      job: 'Software Engineer',
    };

    // fetch('https://reqres.in/api/users', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(data),
    // })
    //   .then(response => response.json())
    //   .then(json => {
    //     console.log(json);
    //     setDataJob(json);
    //   });

    axios
      .post('https://reqres.in/api/users', data)
      .then(result => {
        console.log('result:', result);
        setDataJob(result.data);
      })
      .catch(err => console.log('err:', err));
  };
  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>Axios</Text>
      <Button title="GET DATA" onPress={getData} />
      <Text>Response Get Data</Text>
      {dataUser.avatar.length > 0 && (
        <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
      )}
      <Text>{`${dataUser.first_name} ${dataUser.last_name}`}</Text>
      <Text>{dataUser.email}</Text>
      <View style={styles.line} />
      <Button title="POST DATA" onPress={postData} />
      <Text>Response Post Data</Text>
      <Text>{dataJob.name}</Text>
      <Text>{dataJob.job}</Text>
    </View>
  );
};

export default CallApiAxios;
const styles = StyleSheet.create({
  wrapper: {padding: 20},
  textTitle: {textAlign: 'center'},
  line: {backgroundColor: 'black', height: 2, marginVertical: 20},
  avatar: {width: 100, height: 100, borderRadius: 100},
});
