import React, {Component, useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

const Counter = () => {
  const [number, setNumber] = useState(0);
  return (
    <View>
      <Text style={styles.textTitle}>{number}</Text>
      <Button title="Kurang" onPress={() => setNumber(number - 1)} />
      <Button title="Tambah" onPress={() => setNumber(number + 1)} />
    </View>
  );
};

class CounterClass extends Component {
  state = {
    number: 0,
  };
  render() {
    return (
      <View>
        <Text style={styles.textTitle}>{this.state.number}</Text>
        <Button
          title="Kurang"
          onPress={() => this.setState({number: this.state.number - 1})}
        />
        <Button
          title="Tambah"
          onPress={() => this.setState({number: this.state.number + 1})}
        />
      </View>
    );
  }
}

const StateDinamis = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.textTitle}>State Dinamis</Text>
      <Text style={styles.textTitle}>Functional Component (Hooks)</Text>
      <Counter />
      <Text style={styles.textTitle}>Class Component</Text>
      <CounterClass />
    </View>
  );
};

export default StateDinamis;
const styles = StyleSheet.create({
  wrapper: {padding: 20},
  textTitle: {textAlign: 'center'},
});
