import React, {Component, useEffect, useState} from 'react';
import {Text, View, Image} from 'react-native';

// class Flexbox extends Component {
//   constructor(props) {
//     super(props);
//     console.log('==> constructor');
//     this.state = {
//       subscriber: 200,
//     };
//   }

//   componentDidMount() {
//     console.log('==> component did mount');
//     setTimeout(() => {
//       this.setState({
//         subscriber: 400,
//       });
//     }, 2000);
//   }

//   componentDidUpdate() {
//     console.log('==> component did update');
//   }

//   componentWillUnmount() {
//     console.log('==> console will unmount');
//   }

//   render() {
//     console.log('==> render');
//     return (
//       <View>
//         <View
//           style={{
//             flexDirection: 'row',
//             backgroundColor: '#c8d6e5',
//             alignItems: 'center',
//             justifyContent: 'space-between',
//           }}>
//           {/* <View style={{backgroundColor: '#ee5253', flex: 1, height: 50}} />
//         <View style={{backgroundColor: '#feca57', flex: 1, height: 50}} />
//         <View style={{backgroundColor: '#1dd1a1', flex: 2, height: 50}} />
//         <View style={{backgroundColor: '#5f27cd', flex: 3, height: 50}} /> */}
//           <View style={{backgroundColor: '#ee5253', width: 50, height: 50}} />
//           <View style={{backgroundColor: '#feca57', width: 50, height: 50}} />
//           <View style={{backgroundColor: '#1dd1a1', width: 50, height: 50}} />
//           <View style={{backgroundColor: '#5f27cd', width: 50, height: 50}} />
//         </View>
//         <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
//           <Text>Home</Text>
//           <Text>Products</Text>
//           <Text>Customers</Text>
//           <Text>About</Text>
//           <Text>Contact</Text>
//           <Text>Profile</Text>
//         </View>
//         <View
//           style={{flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
//           <Image
//             source={{
//               uri: 'https://yt3.ggpht.com/ytc/AAUvwnjrI1dxZo2MHAoJ18uw1Kyt3eTHLAeYrBz4WB7kNw=s88-c-k-c0x00ffffff-no-rj',
//             }}
//             style={{width: 100, height: 100, borderRadius: 50, marginRight: 15}}
//           />
//           <View>
//             <Text style={{fontSize: 20, fontWeight: 'bold'}}>
//               Muhtarom Zain
//             </Text>
//             <Text>{this.state.subscriber}K subscribers</Text>
//           </View>
//         </View>
//       </View>
//     );
//   }
// }

const Flexbox = () => {
  const [subscriber, setSubcriber] = useState(200);

  // menggunakan use effect terpisah
  // useEffect(() => {
  //   console.log('did mount');
  // }, []);

  // useEffect(() => {
  //   console.log('did update');
  //   setTimeout(() => {
  //     setSubcriber(400);
  //   }, 2000);
  // }, [subscriber]);

  // menggunakan satu use effect
  useEffect(() => {
    console.log('did mount');
    setTimeout(() => {
      setSubcriber(400);
    }, 2000);
    return () => {
      console.log('did update');
    };
  }, [subscriber]);
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#c8d6e5',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        {/* <View style={{backgroundColor: '#ee5253', flex: 1, height: 50}} />
        <View style={{backgroundColor: '#feca57', flex: 1, height: 50}} />
        <View style={{backgroundColor: '#1dd1a1', flex: 2, height: 50}} />
        <View style={{backgroundColor: '#5f27cd', flex: 3, height: 50}} /> */}
        <View style={{backgroundColor: '#ee5253', width: 50, height: 50}} />
        <View style={{backgroundColor: '#feca57', width: 50, height: 50}} />
        <View style={{backgroundColor: '#1dd1a1', width: 50, height: 50}} />
        <View style={{backgroundColor: '#5f27cd', width: 50, height: 50}} />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Text>Home</Text>
        <Text>Products</Text>
        <Text>Customers</Text>
        <Text>About</Text>
        <Text>Contact</Text>
        <Text>Profile</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
        <Image
          source={{
            uri: 'https://yt3.ggpht.com/ytc/AAUvwnjrI1dxZo2MHAoJ18uw1Kyt3eTHLAeYrBz4WB7kNw=s88-c-k-c0x00ffffff-no-rj',
          }}
          style={{width: 100, height: 100, borderRadius: 50, marginRight: 15}}
        />
        <View>
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>Muhtarom Zain</Text>
          <Text>{subscriber}K subscribers</Text>
        </View>
      </View>
    </View>
  );
};

export default Flexbox;
